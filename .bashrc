#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias hack-nasa='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
alias ls='ls --color=auto'
alias config='/usr/bin/git --git-dir=$HOME/dotfiles-laptop/ --work-tree=$HOME'
PS1='[\u@\h \W]\$ '
